/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
    При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/ 

const head = document.querySelector('head');

const darkTheme = document.createElement('link')
darkTheme.href = './CSS/dark.css';
darkTheme.rel = 'stylesheet'

let wasDark = sessionStorage.getItem('wasDark');
let isDark = wasDark || false;
if(isDark)
{
    head.append(darkTheme);
    isDark--;
}

const switchButton = document.querySelector('.switch');
switchButton.addEventListener('click', () => {
    sessionStorage.setItem('wasDark', isDark);

    if(isDark === false) 
    {
        head.append(darkTheme);
        isDark = true;
    }
    else 
    {
        darkTheme.remove();
        isDark = false;
    }
    console.log(isDark);
})